<?php
defined('BASEPATH') OR exit('No direct script access allowed');

interface iPage {

	public function load($identity);
	public function footer(string $identity);

}

class mPage extends CI_Model implements iPage {

	public $data = array();

	public function load($identity) {
		$queryString = "SELECT * FROM cms_page WHERE ";
		if (is_integer($identity)) {
			$queryString .= "page_id = $identity";
		} elseif (is_string($identity)) {
			$queryString .= "url = '$identity'";
		}
		$query = $this->db->query($queryString);
		$this->data = $query->row();
	}

	public function head(string $identity = '') {
		$this->load->view('pages/parts/head' . $identity . '.php');
	}

	public function footer(string $identity = '') {
		$this->load->view('pages/parts/footer' . $identity . '.php');
	}

}