<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->mPage->head(); ?>
<div class="container">
	<div class="row">
		<div class="col">
			<?php if ($header): ?>
				<h2 class="text-primary text-left">
					<?php echo $header; ?>
				</h2>
			<?php endif; ?>
			
			 
			
		</div>
	</div>
	
	<div class="row">
		<div class="col-6">
			<?php if ($content): ?>

				<main>
					<?php echo htmlspecialchars_decode($content); ?>
						
				</main>
			<?php endif; ?>
		</div>
		<div class="col-6">
			<?php $this->mBlocks->load('google-map')->toHTML(); ?>
		</div>		
			 
	</div>
</div>
<?php $this->mPage->footer(); ?>